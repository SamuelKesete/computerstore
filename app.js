

const laptopsElement = document.getElementById("laptops");
const feturesElement = document.getElementById("fetures");
const bankButton = document.getElementById("bank");
const workButton = document.getElementById("work");
const saleryElement = document.getElementById("salery");
const loanButton = document.getElementById("loan");
const priceElement = document.getElementById("price");
const specsElement = document.getElementById("specs");
const titleElement = document.getElementById("title");
const idelement = document.getElementById("id");
const imgElement = document.getElementById("img");
const bankElement = document.getElementById("balance");
const debtElement = document.getElementById("debt");
const paybackButton = document.getElementById("payback");
const buynowElement = document.getElementById("buynow");




//declaration of variables
let work = 0;
let transfer = 0;
let Loan = 0;
let balance = 0;
let laptops = [];
let add = [];

//Work to make money function
const workFunction = () => {
    //every time you work you get a hundred SEK
    work += 100;
    //payments placeholder
    saleryElement.innerText = `${work}`;
}

//Bank function checks if there are debts then it will deduct 10% and transfer to the loan 
//and the rest of the money to the bank
//however if there are no debts, it will transfer the money to the bank.
const bankFunc = () => {

    if (Loan > 0) {
        if (Loan - work * 0.1 < 0) {
            work -= Loan;
            Loan = 0
            transfer += salary;
            Loan = 0;

        } else {
            balance += work * 0.9;
            Loan = Loan - work * 0.1;
            work = 0;
            bankElement.innerText = balance;
            saleryElement.innerText = work;
            debtElement.innerText = Loan;
        }
    }
    else {
        transfer += work
        work = 0;
        saleryElement.innerText = `${work}`;
        balance += transfer;
        bankElement.innerText = balance;
        transfer = 0;

    }

}

// Get loan function checks if lon is less than or equal to 0, 
//then checks that you can only take bubel as much loan than what is in the bank,
//When the loan is executed then the payback button will appear and 
//if you try to borrow more than once then the loan will not be executed
const getLoan = e => {

    if (Loan <= 0 || Number.isNaN(Loan)) {
        Loan = prompt("Enter how mutch you want to loan from the bank");
        Loan = parseInt(Loan);
        balance = parseInt(bankElement.innerText);
        if (balance * 2 >= Loan) {
            balance += parseInt(Loan);
            bankElement.innerText = parseInt(balance);
            debtElement.innerText += parseInt(Loan);
            paybackButton.style.display = "block";


        } else {
            alert("The value is high");
            Loan = 0;

        }
    } else {
        alert("You must first pay off your debt before you can make a new loan");

    }

}

// handelpayback function handel the pay buttonfunction 
//paybackfunction will first check that the loans are not less than or equal to salery / work.
//then we do calculation balance + work and - the loan-
//this is done to be able to calculate the payback button 
// if there is debt then the whole loan can be paid off 
const handelpayback = () => {
    Loan = parseInt(debtElement.innerText);
    if (Loan <= work) {
        if (Loan == work) {
            Loan = 0;
        }
        else {
            balance = balance + work - Loan;
            Loan = 0;
        }
        work = 0;
        debtElement.innerText = Loan;
        saleryElement.innerText = work;
        bankElement.innerText = balance;
        console.log("low");
    } else {
        Loan -= work;
        work = 0;
        debtElement.innerText = Loan;
        saleryElement.innerText = balance;

    }
    console.log("payback button ");

}

// in it has the part so we fetch the specified apie
//then we check if the json file responds and then we declare data as laptops 
//then we create a new function that shows if time we change the cumputers shows the info

fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    priceElement.innerText = laptops[0].price;
    feturesElement.innerText = laptops[0].description;
    specsElement.innerText = laptops[0].specs;
    idelement.innerText = laptops[0].id;
    titleElement.innerText = laptops[0].title;
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image;
    console.log(imgElement);
}
// in this part we create the dropdown option by targeting the "id" 
// then show the result by the title
const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    console.log(laptopElement);
    laptopsElement.appendChild(laptopElement);
}

// this function fires off each element we select from the dropdown, or each time we chnges 
// by targeting the select element 
const handelLaptopMenuChange = e => {
    const selectlaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = selectlaptop.price;
    feturesElement.innerText = selectlaptop.description;
    specsElement.innerText = selectlaptop.specs;
    idelement.innerText = " ID Number " + selectlaptop.id;
    titleElement.innerText = selectlaptop.title;
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectlaptop.image;
}

// The handlePay function takes care of clicks the buyNow action 
// If the bank is less komputer price, then the user does not have the opportunity to buy  
//If the bank contains more money than the computer costs then the purchase will be made
const handlePay = () => {

    price = parseInt(priceElement.innerText);

    if (balance <= 0 || balance < price) {
        alert("You do not have enough money in your account");
    } else {
        balance -= price;
        alert("Congratulations your purchase was successful");
        bankElement.innerText = parseInt(balance);
    }
}

// addEventListener for the buttons to be clickable and perform the execution
buynowElement.addEventListener("click", handlePay);
workButton.addEventListener("click", workFunction);
bankButton.addEventListener("click", bankFunc);
paybackButton.addEventListener("click", handelpayback);
loanButton.addEventListener("click", getLoan);
laptopsElement.addEventListener("click", handelLaptopMenuChange);




